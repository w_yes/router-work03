import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

// 一级
import Index from './views/Index.vue'
import About from './views/About.vue'
import Contacts from './views/Contacts.vue'

// 二级
import All from './views/second/All.vue'
import Bob from './views/second/Bob.vue'
import Alice from './views/second/Alice.vue'

// 三级
import Blog from './views/second/third/Blog.vue'
import Fax from './views/second/third/Fax.vue'


import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
  { path: '/', component: Index },
  { path: '/index', component: Index },
  { path: '/about', component: About },
  {
    path: '/contacts', component: Contacts, children: [
      { path: '/all', component: All },
      {
        path: '/bob', component: Bob, children: [
          { path: '/blog', component: Blog },
          { path: '/fax', component: Fax },
        ]
      },
      { path: '/alice', component: Alice },
    ]
  },
]
const router = new VueRouter({
  routes
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
